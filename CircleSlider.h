//
//  VertSlider.h
//  CustomSlidersProject
//
//  Created by FirstMac on 22.01.14.
//  Copyright (c) 2014 Nestline. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSliderDelegate.h"

@interface CircleSlider : UIView
@property (nonatomic) CGFloat minValue;
@property (nonatomic) CGFloat maxValue;
@property (nonatomic, weak) id<CustomSliderDelegate> delegate;

@property (nonatomic, weak) IBOutlet UIImageView* cap;
@property (weak, nonatomic) IBOutlet UIImageView *inactiveArcSlider;


@end
