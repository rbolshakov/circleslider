//
//  VertSlider.m
//  CustomSlidersProject
//
//  Created by FirstMac on 22.01.14.
//  Copyright (c) 2014 Nestline. All rights reserved.
//
#define HITLIMIT 200

#import "CircleSlider.h"
#import "HintManager.h"
@interface CircleSlider()
{
    BOOL sliding;
    CALayer *maskLayer;
    CAShapeLayer *maskSublayer;
    
    CGPoint center;
    CGFloat radius;
}


@property (nonatomic, weak) IBOutlet UIImageView* fillView;
@property (nonatomic, weak) IBOutlet UILabel* resultLabel;
@property (nonatomic) NSInteger value;
@end

@implementation CircleSlider


CGFloat fullAngle = 80 * M_PI / 180;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)awakeFromNib
{
    sliding = NO;
    self.minValue = 20;
    self.maxValue = 150;
    
    radius = 107;
    
    self.cap.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    maskLayer = [CALayer layer];
    maskLayer.bounds = self.fillView.layer.bounds;
    maskLayer.anchorPoint = CGPointMake(0, 0);
    maskLayer.position = CGPointMake(-31, -18);
    maskLayer.backgroundColor = [[UIColor clearColor]CGColor];
    
    maskSublayer = [CAShapeLayer layer];
    maskSublayer.bounds = maskLayer.bounds;
    maskSublayer.anchorPoint = CGPointMake(0, 0);
    maskSublayer.position = CGPointMake(0, 0);
    maskSublayer.fillColor = [[UIColor redColor]CGColor];
    maskSublayer.strokeColor = [[UIColor redColor]CGColor];
    maskSublayer.backgroundColor = [[UIColor clearColor]CGColor];
    
    [maskLayer addSublayer:maskSublayer];
    
    self.fillView.layer.mask = maskLayer;
    //[self.fillView.layer addSublayer:maskLayer];
    CGPoint imagePos = self.fillView.frame.origin;
    
    center = CGPointMake(imagePos.x + self.fillView.frame.size.width / 2, imagePos.y - 74);
    [self setGradient:0.0];
}

- (void)setGradient: (CGFloat)angle
{
    float b = (angle / fullAngle);
    float a = asinf(self.cap.frame.size.width / radius);
    float leftCorrection = a * (b - 0.5);
    float rightCorrection = a/2;
    
    CGFloat internalAngle = angle;
    if (angle == fullAngle)
        internalAngle -= 0.001;
    maskSublayer.path = [self pathFrom:M_PI / 2 - fullAngle / 2 - rightCorrection to: M_PI / 2 + fullAngle / 2 - internalAngle - leftCorrection];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SliderActive" object:self];
    NSLog(@"BEGAN");
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];
    CGRect hitRect = self.cap.frame;
    hitRect.origin.x -= HITLIMIT;
    hitRect.origin.y -= HITLIMIT;
    hitRect.size.width += (HITLIMIT * 2);
    hitRect.size.height += (HITLIMIT * 2);
    
    if (CGRectContainsPoint(hitRect, location))
    {
        sliding = YES;
        //NSLog(@"Start sliding");
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];
    if (sliding)
    {
        CGFloat r = sqrt((location.y - center.y) * (location.y - center.y) + (location.x - center.x) * (location.x - center.x));
        
        CGFloat angle = asinf((location.x - center.x) / r);
        if (angle < -fullAngle / 2) angle = -fullAngle / 2;
        if (angle > fullAngle / 2)  angle = fullAngle / 2;
        
        if (isnan(angle)) return;
        
        CGFloat finalY = radius * cosf(angle) + center.y;
        CGFloat finalX = radius * sinf(angle) + center.x;
        
        CGPoint position = self.cap.layer.position;
        position.y = finalY;
        position.x = finalX;
        self.cap.layer.position = position;
        
        //NSLog(@"ratio = %f", (angle + fullAngle/2) / fullAngle);
        
        self.resultLabel.text = [NSString stringWithFormat:@"%.1f", self.minValue + (angle + fullAngle / 2) / fullAngle * (self.maxValue - self.minValue)];
        [self setGradient:angle + fullAngle/2];
        [self.delegate slider:self didSelectValue:self.minValue + (angle + fullAngle / 2) / fullAngle * (self.maxValue - self.minValue)];
    }
}

- (CGPathRef)pathFrom:(CGFloat)startAngle to:(CGFloat)endAngle
{
    CGMutablePathRef arc = CGPathCreateMutable();
    CGPathAddArc(arc, NULL,
                 center.x, center.y,
                 radius,
                 startAngle,
                 endAngle,
                 YES);
    CGPathAddLineToPoint(arc, NULL, center.x, center.y);
    return arc;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"Cancelled");
    sliding = NO;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SliderInactive" object:self];
    //NSLog(@"Ended");
    if ([HintManager sharedManager].nextHint < 2)
        [[HintManager sharedManager]showNextHint];
    
    sliding = NO;
}
@end
